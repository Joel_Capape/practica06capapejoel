package clases;

import java.util.ArrayList;

//Declaro los atributos

public class Persona {
	String nombre;
	String apellido;
	int edad;
	Equipo equipoTitular;

	public Persona() {
		this.nombre = "";
		this.apellido = "";
		this.edad = 0;
	}

//Creo los constructores

	public Persona(String apellido, int edad) {
		this.apellido = apellido;
		this.edad = edad;
	}

	public Persona(String nombre, String apellido, int edad) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
	}
//Creo los getters y setters

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public Equipo getEquipoTitular() {
		return equipoTitular;
	}

	public void setEquipoTitular(Equipo equipoTitular) {
		this.equipoTitular = equipoTitular;
	}

//Creo el metodo toString
	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + ", su equipo favorito es="
				+ equipoTitular + "]";
	}

//metodo propio de la superclase que me ordena los nombres alfabeticamente de forma ascendente
	public void ordenarNombres(ArrayList<Persona> lista) {
		ArrayList<Persona> listaPersonas = lista;
		for (int i = 0; i < listaPersonas.size() - 1; i++) {
			for (int j = i + 1; j < listaPersonas.size(); j++) {
				if (listaPersonas.get(i).getNombre().compareTo(listaPersonas.get(j).getNombre()) > 0) {
					Persona aux = listaPersonas.get(i);
					listaPersonas.set(i, listaPersonas.get(j));
					listaPersonas.set(j, aux);
				}
			}
		}
		System.out.println("Listado de Personas ordenadas de forma ascendente");
	}
}
