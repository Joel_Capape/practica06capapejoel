package clases;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;

/**
 * 
 * @author Joel Capape Hern�ndez
 *
 */
public class Liga {
	public static Scanner input = new Scanner(System.in);
	/**
	 * Creo los ArrayList
	 */
	private ArrayList<Equipo> listaEquipos;
	private ArrayList<Persona> listaPersonas;
	private ArrayList<Jugador> listaJugadores;
	private ArrayList<Entrenador> listaEntrenadores;

	/**
	 * Creo los constructores
	 */
	public Liga() {
		listaEquipos = new ArrayList<Equipo>();
		listaPersonas = new ArrayList<Persona>();
		listaJugadores = new ArrayList<Jugador>();
		listaEntrenadores = new ArrayList<Entrenador>();
	}

	/**
	 * Doy de alta a los Equipos y posteriormente los a�ado al arraylist de equipos
	 */
	public void altaEquipo() {
		System.out.println("Dame el nombre del equipo");
		String nombre = input.nextLine();
		System.out.println("Dame el nombre del patrocinador");
		String patrocinador = input.nextLine();
		System.out.println("Dame el nombre del estadio");
		String estadio = input.nextLine();
		Equipo altaEquipo = new Equipo(nombre, patrocinador, estadio);
		listaEquipos.add(altaEquipo);
	}

	/**
	 * Doy de alta a los Personas y posteriormente los a�ado al arraylist de
	 * personas
	 */
	public void altaPersona() {
		System.out.println("Dame el nombre");
		String nombre = input.nextLine();
		System.out.println("Dame el apellido");
		String apellido = input.nextLine();
		System.out.println("Dame la edad");
		int edad = input.nextInt();
		input.nextLine();
		Persona altaPersona = new Persona(nombre, apellido, edad);
		listaPersonas.add(altaPersona);
	}

	/**
	 * Doy de alta los jugadores y posteriormente los a�ado a la lista Jugadores
	 */
	public void altaJugador() {
		System.out.println("Dame el nombre del jugador");
		String nombre = input.nextLine();
		System.out.println("Dame el apellido del jugador");
		String apellido = input.nextLine();
		System.out.println("Dame la edad del jugador");
		int edad = input.nextInt();
		input.nextLine();
		System.out.println("La posici�n en la que puede jugar son: Portero, Defensa, Mediocentro o Delantero");
		String posicion = input.nextLine();
		System.out.println("Dame el dorsal del jugador");
		int dorsal = input.nextInt();
		System.out.println("Dime el n�mero de goles");
		int nGoles = input.nextInt();
		input.nextLine();
		Jugador altaJugador = new Jugador(nombre, apellido, edad, posicion, dorsal, nGoles);
		listaJugadores.add(altaJugador);
	}

	/**
	 * 
	 * @return precio que es la variable que hace saltar a esta excepci�n que he
	 *         creado para controlar cuando se pone un punto de vez de coma o se
	 *         mete cualquier otro caracter que no es double
	 */
	public double rellenarPrecio() {
		boolean controlador = false;
		double precio = 0;
		do {
			try {
				System.out.println("Introduce el sueldo del entrenador");
				precio = input.nextDouble();
				controlador = false;
			} catch (InputMismatchException a) {
				System.out.println("El sueldo tiene un formato no v�lido");
				controlador = true;
				input.nextLine();
			}
		} while (controlador);
		return precio;
	}

	/**
	 * Doy de alta al entrenador y lo a�ado a la lista entrenadores
	 */
	public void altaEntrenador() {
		System.out.println("Dame el nombre del entrenador");
		String nombre = input.nextLine();
		System.out.println("Dame el apellido del entrenador");
		String apellido = input.nextLine();
		System.out.println("Dame la edad del entrenador");
		int edad = input.nextInt();
		input.nextLine();
		System.out
				.println("El estilo de juego que puede jugar es: Ofensivo, Ultra-Ofensivo, Defensivo, Ultra-Defensivo");
		String estiloDeJuego = input.nextLine();
		System.out.println("La personalidad del entrenador puede ser: Agresivo, Tranquilo, Pasivo");
		String personalidad = input.nextLine();
		double sueldo = rellenarPrecio();
		input.nextLine();
		Entrenador altaEntrenador = new Entrenador(nombre, apellido, edad, estiloDeJuego, personalidad, sueldo);
		listaEntrenadores.add(altaEntrenador);
	}

	/**
	 * Aqu� llamo al metodo propio de la subclase 1 que es Jugador. El metodo me
	 * coge un jugador aleatorio del arraylist y me dice que si marca gol o no
	 */
	public void marcarGol() {
		Jugador miJugador = new Jugador();
		miJugador.marcarGol(listaJugadores);
	}

	/**
	 * Aqu� llamo al metodo propio de la superclase que es Persona. El metodo me ordena los nombres alfabeticamente
	 */
	public void ordenarNombres() {
		Persona miPersona = new Persona();
		miPersona.ordenarNombres(listaPersonas);
	}

	/**
	 * Aqu� llamo al metodo propio de la subclase 2 que es Entrenador. El metodo me ense�a el entrenador mejor pagado
	 */
	public void mayorSueldo() {
		Entrenador miEntrenador = new Entrenador();
		miEntrenador.mayorSueldo(listaEntrenadores);
	}

	/**
	 * Creo el metodo de listar equipo
	 */
	public void listarEquipo() {
		for (Equipo equipos : listaEquipos) {
			System.out.println(equipos);
		}
	}

	/**
	 * Creo el metodo de listar personas
	 */
	public void listarPersonas() {
		for (Persona e : listaPersonas) {
			System.out.println(e);
		}
	}

	/**
	 * Creo el metodo de listar Jugadores
	 */
	public void listarJugadores() {
		for (Jugador jugadores : listaJugadores) {
			System.out.println(jugadores);
		}
	}

	/**
	 * Creo el metodo de listar entrenadores
	 */
	public void listarEntrenadores() {
		for (Entrenador entrenadores : listaEntrenadores) {
			System.out.println(entrenadores);
		}
	}

	/**
	 * 
	 * @return nombreEquipo lo devuelve para posteriomente poder utilizarlo en el
	 *         metdo buscar Equipo
	 */
	public String introducirNombreEquipo() {
		System.out.println("Dime el nombre del equipo que hay que buscar");
		String nombreEquipo = input.nextLine();
		return nombreEquipo;
	}

	/**
	 * 
	 * @param nombreEquipo que lo uso para saber que si el nombre del equipo es
	 *                     igual al get
	 * @return equipos lo devuelve con el parametro que le hemos introducido
	 */
	public Equipo buscarEquipo(String nombreEquipo) {
		for (Equipo equipos : listaEquipos) {
			if (equipos != null && equipos.getNombreEquipo().equals(nombreEquipo)) {
				return equipos;
			}
		}
		return null;
	}

	/**
	 * 
	 * @return nombrePersona lo devuelve para posteriomente poder utilizarlo en el
	 *         metdo buscar Persona
	 */
	public String introducirNombrePersona() {
		System.out.println("Dime el nombre de la persona que hay que buscar");
		String nombrePersona = input.nextLine();
		return nombrePersona;
	}

	/**
	 * 
	 * @param nombrePersona que lo uso para saber que si el nombre de la persona es
	 *                      igual al get
	 * @return nombre lo devuelve con el parametro que le hemos introducido
	 */
	public Persona buscarPersona(String nombre) {
		for (Persona personas : listaPersonas) {
			if (personas != null && personas.getNombre().equals(nombre)) {
				return personas;
			}
		}
		return null;
	}

	/**
	 * 
	 * @return nombreJugador devuelve el nombre que le hemos introducido por teclado
	 */
	public String introducirNombreDeJugador() {
		System.out.println("Dime el nombre del jugador que hay que buscar");
		String nombreJugador = input.nextLine();
		return nombreJugador;
	}

	/**
	 * 
	 * @param nombre que le introducimos al metodo para buscar al Jugador
	 * @return jugadores que comparta el parametro introducido
	 */
	public Jugador buscarJugador(String nombre) {
		for (Jugador jugadores : listaJugadores) {
			if (jugadores != null && jugadores.getNombre().equals(nombre)) {
				return jugadores;
			}
		}
		return null;
	}

	/**
	 * 
	 * @return nombreEntrenador devuelvo el nombre del entrenador que le paso por
	 *         teclado
	 */
	public String nombreDeEntrenador() {
		System.out.println("Dime el nombre del entrenador que hay que buscar");
		String nombreEntrenador = input.nextLine();
		return nombreEntrenador;
	}

	/**
	 * 
	 * @param nombre que le paso por parametro para usar en el metodo buscar
	 *               Entrenador
	 * @return entrenador que comparte el parametro introducido
	 */
	public Entrenador buscarEntrenador(String nombre) {
		for (Entrenador entrenador : listaEntrenadores) {
			if (entrenador != null && entrenador.getNombre().equals(nombre)) {
				return entrenador;
			}
		}
		return null;
	}

	/**
	 * Este metodo me asigna un equipo a una persona
	 * 
	 * @param nombreEquipo  que recoge el nombre de un equipo
	 * @param nombrePersona que recoge el nombre de la persona
	 */
	public void asignarEquipoPersona(String nombreEquipo, String nombrePersona) {
		Equipo equipo = buscarEquipo(nombreEquipo);
		Persona persona = buscarPersona(nombrePersona);
		persona.setEquipoTitular(equipo);
	}

	/**
	 * Metodo que me lista aquellas personas que tienen el equipo asignado
	 * 
	 * @param nombre parametro que le paso al metodo con el nombre del equipo
	 */
	public void listarEquiposDePersonas(String nombre) {
		for (Persona personas : listaPersonas) {
			if (personas.getEquipoTitular() != null && personas.getEquipoTitular().getNombreEquipo().equals(nombre)) {
				System.out.println(personas);
			}
		}
	}

	/**
	 * 
	 * @return Devuelvo el nombre del equipo que quiero eliminar
	 */
	public String introducirEquipoEliminar() {
		System.out.println("Dame el nombre del equipo para eliminar");
		String equipo = input.nextLine();
		return equipo;
	}

	/**
	 * 
	 * @param nombreEquipo parametro que le paso al metodo eliminar equipo y si
	 *                     coincide me elimina al equipo
	 */
	public void eliminarEquipo(String nombreEquipo) {
		Iterator<Equipo> iteradorEquipo = listaEquipos.iterator();
		while (iteradorEquipo.hasNext()) {
			Equipo equipo = iteradorEquipo.next();
			if (equipo.getNombreEquipo().equals(nombreEquipo)) {
				iteradorEquipo.remove();
				System.out.println("El equipo se ha eliminado correctamente");
			}
		}
	}

	/**
	 * 
	 * @return devuelvo el apellido de la persona que quiero eliminar
	 */
	public String introducirApellido() {
		System.out.println("Dame el apellido de la persona que quieres eliminar");
		String apellido = input.nextLine();
		return apellido;
	}

	/**
	 * 
	 * @param apellido parametro que le paso al metodo eliminar persona y si
	 *                 coincide me elimina a la persona
	 */
	public void eliminarPersona(String apellido) {
		Iterator<Persona> iteradorPersona = listaPersonas.iterator();
		while (iteradorPersona.hasNext()) {
			Persona persona = iteradorPersona.next();
			if (persona.getApellido().equals(apellido)) {
				iteradorPersona.remove();
				System.out.println("La persona se ha eliminado correctamente");
			}
		}
	}

	/**
	 * 
	 * @return devuelvo la posicion que le paso por teclado
	 */
	public String introducirPosicion() {
		System.out.println("Dime la posicion del jugador o jugadores  que quieres eliminar");
		String posicion = input.nextLine();
		return posicion;
	}

	/**
	 * 
	 * @param posicion parametro que le paso al metodo eliminar jugador y si
	 *                 coincide la posicion me elimina al jugador
	 */
	public void eliminarJugador(String posicion) {
		Iterator<Jugador> iteradorJugador = listaJugadores.iterator();
		while (iteradorJugador.hasNext()) {
			Jugador jugador = iteradorJugador.next();
			if (jugador.getPosicion().equals(posicion)) {
				iteradorJugador.remove();
				System.out.println("El jugador que ten�a esa posici�n se ha eliminado correctamente");
			}
		}
	}

	/**
	 * 
	 * @return devuelvo la personalidad que le paso por teclado
	 */
	public String introducirPersonalidad() {
		System.out.println("Dime la personalidad que quieres eliminar");
		String personalidad = input.nextLine();
		return personalidad;
	}

	/**
	 * 
	 * @param personalidad parametro que le paso por teclado y si coincide me
	 *                     elimina al entrenador
	 */
	public void eliminarEntrenador(String personalidad) {
		Iterator<Entrenador> iteradorEntrenador = listaEntrenadores.iterator();
		while (iteradorEntrenador.hasNext()) {
			Entrenador entrenador = iteradorEntrenador.next();
			if (entrenador.getPersonalidad().equals(personalidad)) {
				iteradorEntrenador.remove();
				System.out.println("El entrenador con esa personalidad se ha sido eliminados correctamente");
			}
		}
	}

	/**
	 * 
	 * @return equipo1 que me devuelve el nombre del equipo que le paso por teclado
	 */
	public String introducirLocal() {
		System.out.println("Dame el nombre del equipo Local");
		String equipo1 = input.nextLine();
		return equipo1;
	}

	/**
	 * 
	 * @return equipo2 que me devuelve el nombre del equipo que le paso por teclado
	 */
	public String introducirVisitante() {
		System.out.println("Dame el nombre del equipo Visitante");
		String equipo2 = input.nextLine();
		return equipo2;
	}

	/**
	 * 
	 * @param equipo1 le paso la variable como parametro
	 * @param equipo2 le paso la variable como parametro El metodo extra me muestra
	 *                el el resultado de un partido con los nombres de los equipos
	 *                introducidos, el resultado del partido es aleatorio para ello
	 *                he utilizado la clase Math.random()
	 */
	public void resultadoPartido(String equipo1, String equipo2) {
		int golesLocal = (int) (Math.random() * 15);
		int golesVisitante = (int) (Math.random() * 15);
		if (golesLocal > golesVisitante) {
			System.out.println(equipo1 + " " + golesLocal + " - " + equipo2 + " " + golesVisitante);
		} else {
			System.out.println(equipo2 + " " + golesVisitante + " - " + equipo1 + " " + golesLocal);
		}
	}

	/**
	 * 
	 * @return metodo que me devuelve la opci�n del man�
	 */
	public int opciones() {
		System.out.println("_____________________________________________________");
		System.out.println("                         Men�                        ");
		System.out.println("1.-Asignar un Equipo a una Persona");
		System.out.println("2.- �Qu� equipo quieres buscar?");
		System.out.println("3.- �Qu� persona quieres buscar?");
		System.out.println("4.- �Qu� jugador quieres buscar?");
		System.out.println("5.- �Qu� entrenador quieres buscar?");
		System.out.println("6.- Lista de Equipos");
		System.out.println("7.- Lista de Personas");
		System.out.println("8.- Lista de Jugadores");
		System.out.println("9.- Lista de Entrenadores");
		System.out.println("10.- Ordenar alfabeticamente el nombre de personas");
		System.out.println("11.-�El jugador ha marcado gol?");
		System.out.println("12.- El entrenado mejor pagado es...");
		System.out.println("13.-Resultado del partido");
		System.out.println("14.- Eliminar Equipo");
		System.out.println("15.- Eliminar Persona");
		System.out.println("16.- Eliminar Jugador");
		System.out.println("17.- Eliminar Entrneador");
		System.out.println("18.- Saliendo de la aplicaci�n ......");
		System.out.println("_____________________________________________________");
		System.out.println("Introduce una opci�n");
		int opcion = input.nextInt();
		input.nextLine();
		return opcion;

	}

}
