package clases;

import java.util.ArrayList;

//Declaro los atributos y paso los atributos de la superclase a la subclase con el extends
public class Jugador extends Persona {
	String posicion;
	int dorsal;
	int nGoles;

//Creo los constructores
	public Jugador() {

	}

	public Jugador(String nombre, String apellido, int edad, String posicion, int dorsal, int nGoles) {
		super(apellido, edad);
		this.nombre = nombre;
		this.posicion = posicion;
		this.dorsal = dorsal;
		this.nGoles = nGoles;
	}

//Creo los metodos setters y getters
	public String getPosicion() {
		return posicion;
	}

	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}

	public int getDorsal() {
		return dorsal;
	}

	public void setDorsal(int dorsal) {
		this.dorsal = dorsal;
	}

	public int getnGoles() {
		return nGoles;
	}

	public void setnGoles(int nGoles) {
		this.nGoles = nGoles;
	}

	// Creo el toString()
	@Override
	public String toString() {
		return "Jugador [posicion=" + posicion + ", dorsal=" + dorsal + ", nGoles=" + nGoles + ", nombre=" + nombre
				+ ", apellido=" + apellido + ", edad=" + edad + "]";
	}

//metodo propio de la subclase 1 que me recoge un jugador aleatorio y me dice si ese jugador ha marcado gol o no
	public void marcarGol(ArrayList<Jugador> lista) {
		ArrayList<Jugador> listaJugadores = lista;
		String[] vectorNombres = new String[listaJugadores.size()];
		int numeroAleatorio = (int) (Math.random() * 4);
		int numeroAleatorio2 = (int) (Math.random() * 5);
		String nombre = "";
		boolean marcar = false;
		for (int i = 0; i < listaJugadores.size(); i++) {
			vectorNombres[i] = listaJugadores.get(i).getNombre();
		}
		for (int i = 0; i < vectorNombres.length; i++) {
			if (i == numeroAleatorio) {
				nombre = vectorNombres[i];
			}
		}
		for (int i = 0; i < vectorNombres.length; i++) {
			if (i == numeroAleatorio2) {
				marcar = true;
			} else {
				marcar = false;
			}
		}
		if (marcar) {
			System.out.println("El jugador " + nombre + " ha marcado gol");
		} else {
			System.out.println("El jugador " + nombre + " ha fallado una ocasi�n clarisima");
		}
	}
}
