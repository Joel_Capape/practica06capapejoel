package clases;

import java.util.ArrayList;

//declaro los atributos y le paso los atributos de la superclase con el extends
public class Entrenador extends Persona {

	String estiloDeJuego;
	String personalidad;
	double sueldo;

//Creo los constructores
	public Entrenador() {
	}

	public Entrenador(String nombre, String apellido, int edad, String estiloDeJuego, String personalidad,
			double sueldo) {
		super(nombre, apellido, edad);
		this.estiloDeJuego = estiloDeJuego;
		this.personalidad = personalidad;
		this.sueldo = sueldo;
	}

//Creo los getters y setters
	public String getEstiloDeJuego() {
		return estiloDeJuego;
	}

	public void setEstiloDeJuego(String estiloDeJuego) {
		this.estiloDeJuego = estiloDeJuego;
	}

	public String getPersonalidad() {
		return personalidad;
	}

	public void setPersonalidad(String personalidad) {
		this.personalidad = personalidad;
	}

	public double getSueldo() {
		return sueldo;
	}

	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}

//Creo el toString()
	@Override
	public String toString() {
		return "Entrenador [estiloDeJuego=" + estiloDeJuego + ", personalidad=" + personalidad + ", sueldo=" + sueldo
				+ ", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + "]";
	}

//metodo propio de la subclase 2 que me dice el entrenador con mayor sueldo
	public void mayorSueldo(ArrayList<Entrenador> lista) {
		ArrayList<Entrenador> listaEntrenadores = lista;
		int posicion = 0;
		String nombre = "";
		double mayor = 0;
		// rellenamos ambos vectores
		double[] vectorSueldos = new double[listaEntrenadores.size()];
		String[] vectorNombres = new String[listaEntrenadores.size()];
		for (int i = 0; i < listaEntrenadores.size(); i++) {
			vectorSueldos[i] = listaEntrenadores.get(i).getSueldo();
			vectorNombres[i] = listaEntrenadores.get(i).getNombre();
		}
		for (int i = 0; i < vectorSueldos.length; i++) {
			if (mayor < vectorSueldos[i]) {
				mayor = vectorSueldos[i];
				posicion = i;
			}
		}
		for (int i = 0; i < vectorNombres.length; i++) {
			if (i == posicion) {
				nombre = vectorNombres[i];
			}
		}

		System.out.println("El entrenador " + nombre + " es el entrenador mejor pagado con " + mayor + " �");
	}
}
