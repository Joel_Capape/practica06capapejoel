package clases;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class GestionEquipo {
	public static Scanner input = new Scanner(System.in);
//declarar arraylist
	private ArrayList<Equipo> listaEquipos;
	private ArrayList<Persona> listaPersonas;
	private ArrayList<Jugador> listaJugadores;
	private ArrayList<Entrenador> listaEntrenadores;

	// constructores
	public GestionEquipo() {
		listaEquipos = new ArrayList<Equipo>();
		listaPersonas = new ArrayList<Persona>();
		listaJugadores = new ArrayList<Jugador>();
		listaEntrenadores = new ArrayList<Entrenador>();
	}
	// Metodo Alta de las distintas clases
	public void altaEquipo() {
		System.out.println("Dame el nombre del equipo");
		String nombre = input.nextLine();
		System.out.println("Dame el nombre del patrocinador");
		String patrocinador = input.nextLine();
		System.out.println("Dame el nombre del estadio");
		String estadio = input.nextLine();
		Equipo altaEquipo = new Equipo(nombre, patrocinador, estadio);
		listaEquipos.add(altaEquipo);
	}

	public void altaPersona() {
		System.out.println("Dame el nombre");
		String nombre = input.nextLine();
		System.out.println("Dame el apellido");
		String apellido = input.nextLine();
		System.out.println("Dame la edad");
		int edad = input.nextInt();
		input.nextLine();
		Persona altaPersona = new Persona(nombre, apellido, edad);
		listaPersonas.add(altaPersona);
	}

	public void altaJugador() {
		System.out.println("Dame el nombre del jugador");
		String nombre = input.nextLine();
		System.out.println("Dame el apellido del jugador");
		String apellido = input.nextLine();
		System.out.println("Dame la edad del jugador");
		int edad = input.nextInt();
		input.nextLine();
		System.out.println("La posici�n en la que puede jugar son: Portero, Defensa, Mediocentro o Delantero");
		String posicion = input.nextLine();
		System.out.println("Dame el dorsal del jugador");
		int dorsal = input.nextInt();
		System.out.println("Dime el n�mero de goles");
		int nGoles = input.nextInt();
		input.nextLine();
		Jugador altaJugador = new Jugador(nombre, apellido, edad, posicion, dorsal, nGoles);
		listaJugadores.add(altaJugador);
	}

	public double rellenarPrecio() {
		boolean controlador = false;
		double precio = 0;
		do {
			try {
				System.out.println("Introduce el sueldo del entrenador");
				precio = input.nextDouble();
				controlador = false;
			} catch (InputMismatchException a) {
				System.out.println("El sueldo tiene un formato no v�lido");
				controlador = true;
				input.nextLine();
			}
		} while (controlador);
		return precio;
	}

	public void altaEntrenador() {
		System.out.println("Dame el nombre del entrenador");
		String nombre = input.nextLine();
		System.out.println("Dame el apellido del entrenador");
		String apellido = input.nextLine();
		System.out.println("Dame la edad del entrenador");
		int edad = input.nextInt();
		input.nextLine();
		System.out.println("El estilo de juego que puede jugar es: Ofensivo, UltraOfensivo, Defensivo, UltraDefensivo");
		String estiloDeJuego = input.nextLine();
		System.out.println("La personalidad del entrenador puede ser: Agresivo, Tranquilo, Pasivo");
		String personalidad = input.nextLine();
		double sueldo = rellenarPrecio();
		Entrenador altaEntrenador = new Entrenador(nombre, apellido, edad, estiloDeJuego, personalidad, sueldo);
		listaEntrenadores.add(altaEntrenador);
	}

	public void entrenamiento() {
		Jugador miJugador = new Jugador();
		miJugador.marcarGol(listaJugadores);
	}

	public void ordenarNombres() {
		Persona miPersona = new Persona();
		miPersona.ordenarNombres(listaPersonas);
	}

	public void visualizar() {
		for (Persona e : listaPersonas) {
			System.out.println(e);
		}
	}

}
// Metodo listar de las distintas clases
// Metodo buscar de las distintas clases
//Metodo eliminar de las distintas clases
