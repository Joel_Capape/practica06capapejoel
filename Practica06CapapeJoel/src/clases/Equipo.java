package clases;

//Declaro los atributos
public class Equipo {
	String nombreEquipo;
	String patrocinador;
	String nombreEstadio;

	//Declaro los constructores
	public Equipo() {

	}

	
	public Equipo(String nombreEquipo, String patrocinador, String nombreEstadio) {
		this.nombreEquipo = nombreEquipo;
		this.patrocinador = patrocinador;
		this.nombreEstadio = nombreEstadio;
	}
	//Creo los getters y setters
	public String getNombreEquipo() {
		return nombreEquipo;
	}

	public void setNombreEquipo(String nombreEquipo) {
		this.nombreEquipo = nombreEquipo;
	}

	public String getPatrocinador() {
		return patrocinador;
	}

	public void setPatrocinador(String patrocinador) {
		this.patrocinador = patrocinador;
	}

	public String getNombreEstadio() {
		return nombreEstadio;
	}

	public void setNombreEstadio(String nombreEstadio) {
		this.nombreEstadio = nombreEstadio;
	}
	
	// Creo Metodo toString()
	
	@Override
	public String toString() {
		return "Equipo [nombreEquipo=" + nombreEquipo + ", patrocinador=" + patrocinador + ", nombreEstadio="
				+ nombreEstadio + "]";
	}

}
