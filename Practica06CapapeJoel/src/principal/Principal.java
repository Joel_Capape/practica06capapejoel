package principal;

import clases.Liga;

/**
 * 
 * @author Joel Capape Hernández
 *
 */

public class Principal {

	public static void main(String[] args) {
		/**
		 * Creo la instancia de la clase Gestor en mi caso Liga
		 */
		Liga ligaSantander = new Liga();
		/**
		 * Doy de alta 3 elementos de cada clase Equipo
		 */
		System.out.println("Introduce los equipos");
		ligaSantander.altaEquipo();
		ligaSantander.altaEquipo();
		ligaSantander.altaEquipo();
		System.out.println("");
		/**
		 * Doy de alta 3 elementos de la superclase Persona
		 */
		System.out.println("Introduce las personas");
		ligaSantander.altaPersona();
		ligaSantander.altaPersona();
		ligaSantander.altaPersona();
		System.out.println("");
		/**
		 * Doy de alta 4 elementos de la subclase 1 Jugador
		 */
		System.out.println("Introduce los jugadores");
		ligaSantander.altaJugador();
		ligaSantander.altaJugador();
		ligaSantander.altaJugador();
		ligaSantander.altaJugador();
		System.out.println("");
		/**
		 * Doy de alta 3 elementos de la subclase 2 Entrenador
		 */
		System.out.println("Introduce los entrenadores");
		ligaSantander.altaEntrenador();
		ligaSantander.altaEntrenador();
		ligaSantander.altaEntrenador();
		
		/**
		 * A continuación, creo el menu que contendrá los metodos creados en la clase
		 * Liga y los metodos propios de cada subclase y de la Superclase
		 */
		int opcion;
		do {
			opcion=ligaSantander.opciones();
			
			switch(opcion) {
			case 1:
				String nombreEquipo=ligaSantander.introducirNombreEquipo();
				String nombrePersona=ligaSantander.introducirNombrePersona();
				ligaSantander.asignarEquipoPersona(nombreEquipo, nombrePersona);
				System.out.println("Comprobación de que se ha asignado correctamente");
				ligaSantander.listarEquiposDePersonas(nombreEquipo);
				break;
			case 2:
				String equipo=ligaSantander.introducirNombreEquipo();
				System.out.println(ligaSantander.buscarEquipo(equipo));
				break;
			case 3:
				String persona=ligaSantander.introducirNombrePersona();
				System.out.println(ligaSantander.buscarPersona(persona));
				break;
			case 4:
				String nombreDeJugador=ligaSantander.introducirNombreDeJugador();
				System.out.println(ligaSantander.buscarJugador(nombreDeJugador));
				break;
			case 5:
				String entrenador=ligaSantander.nombreDeEntrenador();
				System.out.println(ligaSantander.buscarEntrenador(entrenador));
				break;
			case 6:
				ligaSantander.listarEquipo();
				break;
			case 7:
				ligaSantander.listarPersonas();
				break;
			case 8:
				ligaSantander.listarJugadores();
				break;
			case 9:
				ligaSantander.listarEntrenadores();
				break;
			case 10:
				ligaSantander.ordenarNombres();
				System.out.println("");
				System.out.println("Comprobamos que se han ordenado correctamente");
				ligaSantander.listarPersonas();
				break;
			case 11:
				ligaSantander.marcarGol();
				break;
			case 12:
				ligaSantander.mayorSueldo();
				break;
			case 13:
				String equipo1=ligaSantander.introducirLocal();
				String equipo2=ligaSantander.introducirVisitante();
				ligaSantander.resultadoPartido(equipo1, equipo2);
				break;
			case 14:
				String nEquipo=ligaSantander.introducirEquipoEliminar();
				ligaSantander.eliminarEquipo(nEquipo);
				System.out.println("");
				System.out.println("Comprobación de que se ha eliminado correctamente");
				ligaSantander.listarEquipo();
				break;
			case 15:
				String apellido=ligaSantander.introducirApellido();
				ligaSantander.eliminarPersona(apellido);
				System.out.println("");
				System.out.println("Comprobación de que se ha eliminado correctamente");
				ligaSantander.listarPersonas();
				break;
			case 16:
				String posicion=ligaSantander.introducirPosicion();
				ligaSantander.eliminarJugador(posicion);
				System.out.println("");
				System.out.println("Comprobación de que se ha eliminado correctamente");
				ligaSantander.listarJugadores();
				break;
			case 17:
				String personalidad=ligaSantander.introducirPersonalidad();
				ligaSantander.eliminarEntrenador(personalidad);
				System.out.println("");
				System.out.println("Comprobación de que se ha eliminado correctamente");
				ligaSantander.listarEntrenadores();
				break;
			case 18:
				System.out.println("La aplicación se ha cerrado correctamente");
				System.exit(0);
				break;
				default:
					System.out.println("Opción no encontrada");
			}
		}while(opcion!=18);
	}

}
